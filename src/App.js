import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ToDoForm from './ToDoForm'
import ToDoList from './ToDoList'
import { v4 } from 'uuid'

class App extends Component {

  constructor(props) {
    super(props);
      this.state = {
        todos : []
      }
  }

  submitClicked = (toDoText) => {
    const todoTemp = this.state.todos;
    todoTemp.push({text : toDoText, completed : false, id : v4()});
    this.setState({
      todos : todoTemp
    })
  }

  toDoClicked = (toDo) => {
    const toDoTemp = this.state.todos;
    toDoTemp.map(toDoMap => toDoMap.id == toDo.id ? toDoMap.completed = true : null);
    this.setState({
      todos : toDoTemp
    })
  }

  render() {
    return (
      <div>
      <ToDoForm onSubmitClicked={this.submitClicked}/>
      <ToDoList toDos={this.state.todos} onToDoClicked={this.toDoClicked}/>
      </div>
    );
  }
}

export default App;
