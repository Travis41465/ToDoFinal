import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ToDo from "./ToDo"

class ToDoList extends Component {

    toDoClicked = (toDo) => {
        this.props.onToDoClicked(toDo)
        console.log(toDo.id)
    }

    render() {
        return(
            <ul>
                {this.props.toDos.map((toDo, index) => 
                    <li key={index}><ToDo text={toDo.text} completed={toDo.completed} toDoClicked={() => this.toDoClicked(toDo)}/></li>
                )}
            </ul>
        );
    }

}

export default ToDoList;