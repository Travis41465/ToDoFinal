import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Input, Button } from 'semantic-ui-react';

class ToDo extends Component {
    render() {
        const textStyle=this.props.completed ? 'line-through' : 'none'
        console.log(textStyle)
        return(
            <h4 style={{textDecoration:textStyle}} onClick={this.props.toDoClicked}>{this.props.text}</h4>
        );
    }
}

export default ToDo;