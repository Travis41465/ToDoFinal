import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Input, Button } from 'semantic-ui-react';

class ToDoForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputText : ""
        }
    }

    submitClicked = () => {
        this.props.onSubmitClicked(this.state.inputText);
        this.setState({
            inputText : ""
        })
    }

    onTextChanged = (event) => {
        this.setState({
            inputText : event.target.value
        })
    }

    render() {
        return(
            <div>
                <Input type="text" value={this.state.inputText} onChange={this.onTextChanged}/>
                <Button onClick={this.submitClicked}>Submit</Button>
            </div>
        );
    }

}

export default ToDoForm;